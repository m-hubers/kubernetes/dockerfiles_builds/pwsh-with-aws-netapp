
# Docker image file that describes an Ubuntu20.04 image with PowerShell installed from Microsoft APT Repo
FROM mcr.microsoft.com/powershell:7.2.1-ubuntu-20.04 AS installer-env

# Install dependencies and clean up
RUN apt-get update \
    && apt-get install --no-install-recommends -y \
    # tools/apps we need
		jq \
        python3 \
		python3-pip \
    # Download the Linux package and save it
    && apt-get dist-upgrade -y \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN pwsh -command Install-Module "AWSPowerShell.NetCore,NetApp.ONTAP" -Force

RUN pip install boto3
